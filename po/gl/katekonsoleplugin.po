# translation of katekonsoleplugin.po to galician
# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# mvillarino <mvillarino@users.sourceforge.net>, 2007, 2008, 2009.
# Marce Villarino <mvillarino@gmail.com>, 2009, 2013.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015, 2017.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2018, 2019, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: katekonsoleplugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-09 00:51+0000\n"
"PO-Revision-Date: 2023-08-05 10:05+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.3\n"

#: kateconsole.cpp:56
#, kde-format
msgid "You do not have enough karma to access a shell or terminal emulation"
msgstr ""
"Non ten karma de abondo para acceder a un intérprete de ordes nin a un "
"emulador de terminal."

#: kateconsole.cpp:104 kateconsole.cpp:134 kateconsole.cpp:664
#, kde-format
msgid "Terminal"
msgstr "Terminal"

#: kateconsole.cpp:143
#, kde-format
msgctxt "@action"
msgid "&Pipe to Terminal"
msgstr "&Canalizar ao terminal"

#: kateconsole.cpp:147
#, kde-format
msgctxt "@action"
msgid "S&ynchronize Terminal with Current Document"
msgstr "&Sincronizar a terminal co documento actual"

#: kateconsole.cpp:151
#, kde-format
msgctxt "@action"
msgid "Run Current Document"
msgstr "Executar o documento actual"

#: kateconsole.cpp:156 kateconsole.cpp:513
#, kde-format
msgctxt "@action"
msgid "S&how Terminal Panel"
msgstr "Amosar o panel de &terminal"

#: kateconsole.cpp:162
#, kde-format
msgctxt "@action"
msgid "&Focus Terminal Panel"
msgstr "&Enfocar o panel de terminal"

#: kateconsole.cpp:307
#, kde-format
msgid ""
"Konsole not installed. Please install konsole to be able to use the terminal."
msgstr "Konsole non está instalado. Instale Konsole para usar un terminal."

#: kateconsole.cpp:388
#, kde-format
msgid ""
"Do you really want to pipe the text to the console? This will execute any "
"contained commands with your user rights."
msgstr ""
"Seguro que quere canalizar o texto á consola? Isto executará calquera orde "
"que conteña cos seus permisos de persoa usuaria."

#: kateconsole.cpp:389
#, kde-format
msgid "Pipe to Terminal?"
msgstr "Canalizar ao terminal?"

#: kateconsole.cpp:390
#, kde-format
msgid "Pipe to Terminal"
msgstr "Canalizar ao terminal"

#: kateconsole.cpp:418
#, kde-format
msgid "Sorry, cannot cd into '%1'"
msgstr "Sentímolo, non se pode acceder (cd) a «%1»."

#: kateconsole.cpp:454
#, kde-format
msgid "Not a local file: '%1'"
msgstr "Non é un ficheiro local: «%1»"

#: kateconsole.cpp:487
#, kde-format
msgid ""
"Do you really want to Run the document ?\n"
"This will execute the following command,\n"
"with your user rights, in the terminal:\n"
"'%1'"
msgstr ""
"Seguro que quere executar o documento?\n"
"Isto executará no terminal a seguinte orde,\n"
"cos seus permisos de persoa usuaria:\n"
"«%1»"

#: kateconsole.cpp:494
#, kde-format
msgid "Run in Terminal?"
msgstr "Executar nun terminal?"

#: kateconsole.cpp:495
#, kde-format
msgid "Run"
msgstr "Executar"

#: kateconsole.cpp:510
#, kde-format
msgctxt "@action"
msgid "&Hide Terminal Panel"
msgstr "Agoc&har o panel de terminal"

#: kateconsole.cpp:521
#, kde-format
msgid "Defocus Terminal Panel"
msgstr "Desenfocar o panel de terminal"

#: kateconsole.cpp:522 kateconsole.cpp:523
#, kde-format
msgid "Focus Terminal Panel"
msgstr "Enfocar o panel de terminal"

#: kateconsole.cpp:597
#, kde-format
msgid ""
"&Automatically synchronize the terminal with the current document when "
"possible"
msgstr ""
"Sincronizar &automaticamente a terminal co documento actual cando sexa "
"posíbel"

#: kateconsole.cpp:601 kateconsole.cpp:622
#, kde-format
msgid "Run in terminal"
msgstr "Executar nun terminal"

#: kateconsole.cpp:603
#, kde-format
msgid "&Remove extension"
msgstr "&Retirar a extensión"

#: kateconsole.cpp:608
#, kde-format
msgid "Prefix:"
msgstr "Prefixo:"

#: kateconsole.cpp:616
#, kde-format
msgid "&Show warning next time"
msgstr "&Amosar o aviso a próxima vez."

#: kateconsole.cpp:618
#, kde-format
msgid ""
"The next time '%1' is executed, make sure a warning window will pop up, "
"displaying the command to be sent to terminal, for review."
msgstr ""
"A próxima vez que se execute «%1», asegurarse de que aparece unha xanela de "
"aviso, amosando a orde que se vai enviar ao terminal, para revisala."

#: kateconsole.cpp:629
#, kde-format
msgid "Set &EDITOR environment variable to 'kate -b'"
msgstr "Configurar a variábel de contorno &EDITOR como «kate -b»"

#: kateconsole.cpp:632
#, kde-format
msgid ""
"Important: The document has to be closed to make the console application "
"continue"
msgstr ""
"Importante: O documento debe estar pechado para facer continuar a aplicación "
"de consola"

#: kateconsole.cpp:635
#, kde-format
msgid "Hide Konsole on pressing 'Esc'"
msgstr "Agochar Konsole ao premer Esc"

#: kateconsole.cpp:638
#, kde-format
msgid ""
"This may cause issues with terminal apps that use Esc key, for e.g., vim. "
"Add these apps in the input below (Comma separated list)"
msgstr ""
"Esto pode causar problemas con aplicacións de terminal que usan a tecla Esc, "
"p. ex. vim. Liste esas aplicacións a continuación, separada por comas."

#: kateconsole.cpp:669
#, kde-format
msgid "Terminal Settings"
msgstr "Configuración de terminal"

#. i18n: ectx: Menu (tools)
#: ui.rc:6
#, kde-format
msgid "&Tools"
msgstr "&Utilidades"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Marce Villarino"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "mvillarino@users.sourceforge.net"

#~ msgid "Kate Terminal"
#~ msgstr "Terminal de Kate"

#, fuzzy
#~| msgid "Terminal"
#~ msgid "Terminal Panel"
#~ msgstr "Terminal"

#~ msgid "Konsole"
#~ msgstr "Konsole"

#~ msgid "Embedded Konsole"
#~ msgstr "Konsole incrustada"
